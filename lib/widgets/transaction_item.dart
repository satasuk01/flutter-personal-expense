import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:personal_expense_app/models/transaction.dart';

class TransactionItem extends StatefulWidget {
  const TransactionItem({
    Key? key,
    required this.tx,
    required this.removeTransaction,
  }) : super(key: key);

  final Transaction tx;
  final Function removeTransaction;

  @override
  _TransactionItemState createState() => _TransactionItemState();
}

class _TransactionItemState extends State<TransactionItem> {
  Color? _bgColor;

  @override
  void initState() {
    const availableColors = [
      Colors.red,
      Colors.black,
      Colors.blue,
      Colors.purple,
      Colors.green,
      Colors.amber,
    ];

    _bgColor = availableColors[
        Random().nextInt(availableColors.length)]; // No need to set state
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: _bgColor,
          radius: 30,
          child: Padding(
            padding: EdgeInsets.all(6),
            child: FittedBox(
                child: Text('\$${widget.tx.amount.toStringAsFixed(2)}')),
          ),
        ),
        title: Text(
          widget.tx.title,
          style: Theme.of(context).textTheme.headline6,
        ),
        subtitle: Text(
          DateFormat.yMMMd().format(widget.tx.date),
          style: TextStyle(color: Colors.grey),
        ),
        trailing: MediaQuery.of(context).size.width > 360
            ? TextButton.icon(
                onPressed: () => widget.removeTransaction(widget.tx.id),
                icon: Icon(Icons.delete, color: Colors.grey.shade300),
                label: Text('Delete',
                    style: TextStyle(color: Colors.grey.shade300)))
            : IconButton(
                icon: Icon(Icons.delete, color: Colors.grey.shade300),
                onPressed: () => widget.removeTransaction(widget.tx.id),
              ),
      ),
    );
  }
}
