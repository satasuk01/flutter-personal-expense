import 'package:flutter/material.dart';
import 'package:personal_expense_app/models/transaction.dart';
import 'package:personal_expense_app/widgets/transaction_item.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function removeTransaction;

  TransactionList(this.transactions, this.removeTransaction);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 300,
      child: transactions.isEmpty
          ? LayoutBuilder(builder: (context, constraints) {
              return Column(
                children: <Widget>[
                  Text(
                    "Empty",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  SizedBox(height: 20),
                  Container(
                    height: constraints.maxHeight * 0.6,
                    child: Image.asset(
                      'assets/images/waiting.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              );
            })
          // : ListView.builder(
          //     itemBuilder: (ctx, index) {
          //       final tx = transactions[index];

          //       return TransactionItem(
          //           tx: tx, removeTransaction: removeTransaction);
          //     },
          //     itemCount: transactions.length,
          //   ),
          : ListView(
              children: transactions
                  .map((tx) => TransactionItem(
                        // key: UniqueKey(), // Make the color changes everytime
                        key: ValueKey(tx.id),
                        tx: tx,
                        removeTransaction: removeTransaction,
                      ))
                  .toList(),
            ),
    );
  }
}
